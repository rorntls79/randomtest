package com.kotlin.ex.ktrandomview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kotlin.ex.ktrandomview.di.RowDataInterface
import com.kotlin.ex.ktrandomview.view.Dimension
import com.kotlin.ex.ktrandomview.view.DotView
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainVm @Inject constructor(
	private val dataSource: RowDataInterface
): ViewModel() {
	var dimension = Dimension.X_DIM
	var isGaussian = false


	fun processAdd(dv: DotView, cnt: Int) {
		when {
			dimension == Dimension.X_DIM && isGaussian -> {
				dataSource.getGaussianD1Multi(cnt).forEach {
					dv.addD1Data(it)
				}
			}
			dimension == Dimension.X_DIM && !isGaussian -> {
				dataSource.getRandomD1Multi(cnt).forEach {
					dv.addD1Data(it)
				}
			}
			dimension == Dimension.XY_DIM && isGaussian -> {
				dataSource.getGaussianD2Multi(cnt).forEach {
					dv.addD2Data(it.x, it.y)
				}
			}
			dimension == Dimension.XY_DIM && !isGaussian -> {
				dataSource.getRandomD2Multi(cnt).forEach {
					dv.addD2Data(it.x, it.y)
				}
			}
		}
		dv.invalidate()
	}
}