package com.kotlin.ex.ktrandomview.di

import android.graphics.PointF

class RowDataSource(
	private val r:RandInterface,
	private val g:RandInterface
	): RowDataInterface {

	override fun getRandomD1(): Float = r.randD1Data()

	override fun getRandomD2(): PointF = r.randD2Data()

	override fun getRandomD1Multi(len: Int): ArrayList<Float> = r.randD1DataSet(len)

	override fun getRandomD2Multi(len: Int): ArrayList<PointF> = r.randD2DataSet(len)

	override fun getGaussianD1(): Float = g.randD1Data()

	override fun getGaussianD2(): PointF = g.randD2Data()

	override fun getGaussianD1Multi(len: Int): ArrayList<Float> = g.randD1DataSet(len)

	override fun getGaussianD2Multi(len: Int): ArrayList<PointF> = g.randD2DataSet(len)
}