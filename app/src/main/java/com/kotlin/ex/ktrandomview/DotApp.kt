package com.kotlin.ex.ktrandomview

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class DotApp: Application()