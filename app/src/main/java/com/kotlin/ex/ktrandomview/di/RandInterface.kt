package com.kotlin.ex.ktrandomview.di

import android.graphics.PointF

interface RandInterface {
	fun reset()

	fun randD1Data(): Float

	fun randD2Data(): PointF

	fun randD1DataSet(len: Int): ArrayList<Float>

	fun randD2DataSet(len: Int): ArrayList<PointF>
}