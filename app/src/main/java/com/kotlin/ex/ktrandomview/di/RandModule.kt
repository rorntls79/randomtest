package com.kotlin.ex.ktrandomview.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import javax.inject.Qualifier

@Module
@InstallIn(ViewModelComponent::class)
class RandModule {

	@Provides
	fun provideRand(
		@Random r: RandInterface,
		@Gaussian g: RandInterface
	): RowDataInterface {
		return RowDataSource(r, g)
	}

	@Qualifier
	@Retention(AnnotationRetention.BINARY)
	annotation class Random

	@Qualifier
	@Retention(AnnotationRetention.BINARY)
	annotation class Gaussian

	@Provides
	@Random
	fun provRandom(): RandInterface {
		return RandImpl()
	}

	@Provides
	@Gaussian
	fun provGaussian(): RandInterface {
		return GaussImpl()
	}
}