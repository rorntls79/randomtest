package com.kotlin.ex.ktrandomview.di

import android.graphics.PointF

interface RowDataInterface {
	fun getRandomD1(): Float

	fun getRandomD2(): PointF

	fun getRandomD1Multi(len: Int): ArrayList<Float>

	fun getRandomD2Multi(len: Int): ArrayList<PointF>

	fun getGaussianD1(): Float

	fun getGaussianD2(): PointF

	fun getGaussianD1Multi(len: Int): ArrayList<Float>

	fun getGaussianD2Multi(len: Int): ArrayList<PointF>
}