package com.kotlin.ex.ktrandomview

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.kotlin.ex.ktrandomview.databinding.ActivityMainBinding
import com.kotlin.ex.ktrandomview.view.Dimension
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
	private val binder by lazy {	ActivityMainBinding.inflate(layoutInflater)	}
	private val vm: MainVm by viewModels()


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(binder.root)
		binder.act = this
	}

	fun onDimensionChanged(chkId: Int) {
		if( chkId == R.id.rb_dimen1 ) {
			vm.dimension = Dimension.X_DIM
			binder.dvDot.dimensions.value = Dimension.X_DIM
		}
		else {
			vm.dimension = Dimension.XY_DIM
			binder.dvDot.dimensions.value = Dimension.XY_DIM
		}
	}

	fun onTypeChanged(chkId: Int) {
		vm.isGaussian = chkId == R.id.rb_gauss
	}

	fun onClearClick() {
		binder.dvDot.clearData()
	}

	fun addDot(cnt: Int) {
		vm.processAdd(binder.dvDot, cnt)
	}
}