package com.kotlin.ex.ktrandomview.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.MutableLiveData
import kotlin.collections.set

class DotView: View, LifecycleOwner {
	constructor(ctx: Context) : super(ctx)

	constructor(ctx: Context?, attrs: AttributeSet?) :
			super(ctx, attrs) {
		init()
	}

	constructor(ctx: Context?, attrs: AttributeSet?, defStyle: Int) :
			super(ctx, attrs, defStyle) {
		init()
	}

	constructor(ctx: Context?, attrs: AttributeSet?, defStyle: Int, defStyleRes: Int) :
			super(ctx, attrs, defStyle, defStyleRes) {
		init()
	}



	private val lifecycleReg = LifecycleRegistry(this)

	val dimensions = MutableLiveData(Dimension.X_DIM)
	private lateinit var bgPaint: Paint
	private lateinit var axisPaint: Paint
	private lateinit var dotPaint: Paint

	private val d1Data = HashMap<Int, Int>()
	private val d2Data = ArrayList<Point>()



	private fun init() {
		bgPaint = Paint().apply {
			color = Color.parseColor("#888888")
		}

		axisPaint = Paint().apply {
			color = Color.parseColor("#00ff00")
			strokeWidth = 10f
		}

		dotPaint = Paint().apply {
			color = Color.parseColor("#ff0000")
			strokeWidth = 5f
		}

		dimensions.observe(this) {
			clearData()
		}

		if( isInEditMode ) {
			Log.e("###", "Wow")
		}
	}

	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		lifecycleReg.currentState = Lifecycle.State.STARTED
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		lifecycleReg.currentState = Lifecycle.State.DESTROYED
	}

	override fun onDraw(canvas: Canvas?) {
		super.onDraw(canvas)
		Log.e("###", "vWidth=$width    vHeight=$height")

		canvas?.let {
			// Clear Screen
			it.drawRect(0f, 0f, width.toFloat(), height.toFloat(), bgPaint)

			// Draw axis
			drawAxis(it)

			// Draw Data
			when(dimensions.value) {
				Dimension.X_DIM -> drawD1(canvas)
				Dimension.XY_DIM -> drawD2(canvas)
				else -> { Log.e("###", "What?") }
			}
		}
	}

	private fun drawAxis(canvas: Canvas) {
		// draw default axis(X)
		canvas.drawLine(
			0f,
			(canvas.height/2).toFloat(),
			canvas.width.toFloat(),
			(canvas.height/2).toFloat(),
			axisPaint
		)

		// draw additional axis(Y) by condition
		if(dimensions.value == Dimension.XY_DIM ) {
			canvas.drawLine(
				(canvas.width/2).toFloat(),
				0f,
				(canvas.width/2).toFloat(),
				canvas.height.toFloat(),
				axisPaint
			)
		}
	}

	private fun drawD1(canvas: Canvas) {
		for( k in d1Data.keys ) {
			canvas.drawLine(
				k.toFloat(),
				(height/2).toFloat(),
				k.toFloat(),
				((height/2 - d1Data[k]!!*1).toFloat()),
				dotPaint
			)
		}
	}

	private fun drawD2(canvas: Canvas) {
		for( pt in d2Data ) {
			canvas.drawCircle(pt.x.toFloat(), pt.y.toFloat(), 5f, dotPaint)
		}
	}

	override fun getLifecycle(): Lifecycle {
		return lifecycleReg
	}

	fun addD1Data(v: Float) {
		val sx = scalingToInt(v, width)

		if( d1Data.containsKey(sx) ) {
			d1Data[sx] = d1Data[sx]!!.plus(1)
		}
		else {
			d1Data[sx] = 1
		}
	}

	fun addD2Data(x: Float, y: Float) {
		d2Data.add(Point(scalingToInt(x, width), scalingToInt(y, height)))
	}

	private fun scalingToInt(v: Float, stand: Int): Int = ((((v*100f).toInt()).toFloat()/100f) * stand).toInt()

	fun clearData() {
		d1Data.clear()
		d2Data.clear()

		invalidate()
	}
}

enum class Dimension {
	X_DIM,
	XY_DIM
}