package com.kotlin.ex.ktrandomview.di

import android.graphics.PointF
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class GaussImpl: RandInterface {
	private val rand: Random by lazy { Random(System.currentTimeMillis()) }


	override fun reset() = rand.setSeed(System.currentTimeMillis())

	override fun randD1Data(): Float {
		var r: Float
		while(true) {
			r = rand.nextGaussian().toFloat()
			if( abs(r) > 4f ) continue

			r = (r+4f) / 8f
			break
		}

		return r
	}

	override fun randD2Data(): PointF {
		var x: Float
		var y: Float
		while( true ) {
			x = rand.nextGaussian().toFloat()
			y = rand.nextGaussian().toFloat()
			if( abs(x) > 4f ) continue
			if( abs(y) > 4f ) continue

			x = (x+4f) / 8f
			y = (y+4f) / 8f
			break
		}
		return PointF(x, y)
	}

	override fun randD1DataSet(len: Int): ArrayList<Float> {
		return ArrayList<Float>().apply {
			for( x in 0 until len )
				add(randD1Data())
		}
	}

	override fun randD2DataSet(len: Int): ArrayList<PointF> {
		return ArrayList<PointF>().apply {
			for( x in 0 until len )
				add(randD2Data())
		}
	}
}